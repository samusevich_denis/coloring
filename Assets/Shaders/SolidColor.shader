﻿Shader "2D Shaders/SolidColor"
{
    Properties
    {
        _ColorStandart ("Color", Color) = (1,1,1,1)
    }

    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            struct appdata {
                float4 position : POSITION;
            };

            struct v2f {
                float4 position : SV_POSITION;
            };

            float4 _ColorStandart;

            UNITY_INSTANCING_BUFFER_START(Props)
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)
            UNITY_INSTANCING_BUFFER_END(Props)

            v2f vert (appdata v){
                v2f o;
                o.position = UnityObjectToClipPos(v.position);
                return o;
            }
            
            fixed4 frag (v2f i): SV_Target {
                
                return _ColorStandart*UNITY_ACCESS_INSTANCED_PROP(Props, _Color);
            }
            ENDCG
        }
    }
}