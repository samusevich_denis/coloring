﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPicture : MonoBehaviour
{
    [SerializeField] private ButtonPressed buttonPresed;
    [SerializeField] private int section;
    [SerializeField] private int image;

    public void Load()
    {
        if (buttonPresed.isMouseDrag)
        {
            return;
        }
        GameController.controller.LoadPicture(section, image);
    }

}
