﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScreenLoading : MonoBehaviour
{
    [SerializeField] private GameObject gameObjectCanvas;
    [SerializeField] private float speedFade;
    [SerializeField] private float WaitTimeFade;
    private Image imageLoading;
    private bool isLoad;


    private void Start()
    {
        gameObjectCanvas.SetActive(true);
        imageLoading = gameObjectCanvas.GetComponent<Image>();
        imageLoading.color = Color.white;
        isLoad = true;
        StartCoroutine(FadeoutImage());
        StartCoroutine(WaitLoad(WaitTimeFade));

    }
    private IEnumerator WaitLoad(float time)
    {
        yield return new WaitForSeconds(time);
        isLoad = false;
    }
    private IEnumerator FadeinImage()
    {
        gameObjectCanvas.SetActive(true);
        var colorAlfa = imageLoading.color;
        colorAlfa.a = 0;
        imageLoading.color = colorAlfa;

        while (imageLoading.color.a < 0.99f)
        {
            var color = imageLoading.color;
            color.a = Mathf.Lerp(color.a, 1, speedFade * Time.deltaTime);
            imageLoading.color = color;
            yield return null; 
        }

    }
    private IEnumerator FadeoutImage()
    {

        imageLoading.color = Color.white;
        while (isLoad)
        {
            yield return null;
        }
        while (imageLoading.color.a > 0.01f)
        {
            var color = imageLoading.color;
            color.a = Mathf.Lerp(color.a, 0, speedFade * Time.deltaTime);
            imageLoading.color = color;
            yield return null; ;
        }
        gameObjectCanvas.SetActive(false);

    }

}
