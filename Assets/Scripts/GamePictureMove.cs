﻿
using UnityEngine;

public class GamePictureMove : MonoBehaviour
{
    [SerializeField] private Vector2 padding;
    [SerializeField] private float speedMove;
    [SerializeField] private float speedScale;
    [SerializeField] private bool freezeX;
    [SerializeField] private bool freezeY;
    [SerializeField] private Vector2 sizeElement;
    [SerializeField] private float coefficientSize;
    [SerializeField] private float minSize;
    [SerializeField] private float maxSize;
    private bool isFirstMousePosition;
    private bool isFirstLengthTouch;
    private Camera cameraMain;
    private Vector2 maxDrag;
    private Vector2 minDrag;
    private Vector2 lastOffset;
    private float lastOffsetLength;
    private Vector2 freezePosition;
    private Vector3 lastMousePosition;
    private float lastLengthTouch;
    [SerializeField] bool isScale;
    [SerializeField] private Vector3 point;


    // Start is called before the first frame update
    void Start()
    {
        cameraMain = Camera.main;
        if (freezeY || freezeX)
        {
            freezePosition = transform.position;
        }
        SetMinMaxDrag();
        isFirstMousePosition = true;
             isFirstLengthTouch= true;
}

    public void SetMinMaxDrag()
    {
        var halfSizeScreen = cameraMain.ScreenToWorldPoint(Vector3.zero);
        halfSizeScreen *= -1;
        if (halfSizeScreen.x > sizeElement.x/2 + padding.x)
        {
            minDrag.x = -halfSizeScreen.x + padding.x + sizeElement.x/2;
            maxDrag.x = halfSizeScreen.x - padding.x - sizeElement.x / 2;
        }
        else
        {
            minDrag.x = halfSizeScreen.x- sizeElement.x /2 -padding.x;
            maxDrag.x = -halfSizeScreen.x + sizeElement.x / 2 + padding.x;
        }
        if (halfSizeScreen.y > sizeElement.y/ 2+ padding.y)
        {
            minDrag.y = -halfSizeScreen.y + padding.y + sizeElement.y / 2;
            maxDrag.y = halfSizeScreen.y - padding.y - sizeElement.y / 2; 
        }
        else
        {
            minDrag.y = halfSizeScreen.y - padding.y - sizeElement.y / 2; 
            maxDrag.y = -halfSizeScreen.y + padding.y + sizeElement.y / 2;
        }
    }
    public void MovePicture()
    {
        if (isFirstMousePosition)
        {
 
            lastMousePosition = cameraMain.ScreenToWorldPoint(Input.GetTouch(0).position);
            //lastMousePosition = cameraMain.ScreenToWorldPoint(Input.mousePosition);
        }

        var mousePosition = cameraMain.ScreenToWorldPoint(Input.GetTouch(0).position);
        //var mousePosition = cameraMain.ScreenToWorldPoint(Input.mousePosition);
        var offset = (Vector2)(mousePosition - lastMousePosition);
        offset += lastOffset;

        var position = (Vector2)transform.position;


        var target = position + offset;

        position = Vector2.Lerp(position, target, Time.deltaTime * speedMove);
        lastOffset = target - position;
        if (freezeX)
        {
            position.x = freezePosition.x;
        }
        if (freezeY)
        {
            position.y = freezePosition.y;
        }
        transform.position = position;
        lastMousePosition = cameraMain.ScreenToWorldPoint(Input.GetTouch(0).position);
        //lastMousePosition = cameraMain.ScreenToWorldPoint(Input.mousePosition);
        if (isFirstMousePosition)
        {
            isFirstMousePosition = !isFirstMousePosition;
        }
    }
    private void Update()
    {
        //if (Input.GetMouseButton(0))
        //{
        //    ScalePicture();
        //    return;
        //}
        switch (Input.touchCount)
        {
            case 0:
                ReturnPicture();
                break;
            case 1:
                MovePicture();
                break;
            case 2:
                ScalePicture(); 
                break;
            default:
                return;
        }
    }
    private void ReturnPicture()
    {
        if (!isFirstMousePosition)
        {
            isFirstMousePosition = !isFirstMousePosition;
        }
        if (!isFirstLengthTouch)
        {
            SetMinMaxDrag();
            isFirstLengthTouch = !isFirstLengthTouch;

        }
        var position = (Vector2)transform.position;
        var nextPosition = position.ClampVector2(minDrag, maxDrag);

        position = Vector2.Lerp(position, nextPosition, Time.deltaTime * speedMove);
        transform.position = position;
        if ((position - nextPosition).magnitude < 0.01)
        {
            position = nextPosition;
        }
        transform.position = position;
    }

    private void ScalePicture()
    {
        if (!isFirstMousePosition)
        {
            isFirstMousePosition = !isFirstMousePosition;
        }
        if (isFirstLengthTouch)
        {
            lastLengthTouch = /*isScale?(Input.mousePosition - point).magnitude: */(Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;
        }

        var lengthTouch = /*isScale ? (Input.mousePosition - point).magnitude : */(Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;
        var offset = lengthTouch - lastLengthTouch;
        offset += lastOffsetLength;

        var size = cameraMain.orthographicSize;

        var target = size - offset * coefficientSize;
        target = Mathf.Clamp(target, minSize, maxSize);
        size = Mathf.Lerp(size, target, Time.deltaTime * speedScale);
        lastOffsetLength = target - size;

        cameraMain.orthographicSize = size;
        lastLengthTouch = /*isScale ? (Input.mousePosition - point).magnitude : */(Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;
        if (isFirstLengthTouch)
        {
            isFirstLengthTouch = !isFirstLengthTouch;
        }


    }
}
