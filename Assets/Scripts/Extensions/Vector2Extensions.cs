﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector2Extensions
{
    public static bool pointComparisons(this Vector2 currentVector, Vector2 vector, float degreeOfError)
    {
        return (currentVector - vector).magnitude < degreeOfError;
    }
    public static Vector2 ClampVector2(this Vector2 currentVector, Vector2 vectorMin, Vector2 vectorMax)
    {
        currentVector.x = Mathf.Clamp(currentVector.x, vectorMin.x, vectorMax.x);
        currentVector.y = Mathf.Clamp(currentVector.y, vectorMin.y, vectorMax.y);
        return currentVector;
    }
    public static void SetVector2(this Vector2 currentVector, Vector3 vector)
    {
        currentVector.x = vector.x;
        currentVector.y = vector.y;
    }

}
