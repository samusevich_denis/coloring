﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorFigure : MonoBehaviour
{
    private MeshRenderer renderer;

    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
    }
    public void SetColor(Color color)
    {
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        props.SetColor("_Color", color);
        renderer.SetPropertyBlock(props);
    }
    public void SetMaterial(Material material)
    {
        renderer.sharedMaterial = material;
    }

}
