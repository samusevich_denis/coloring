﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DragPanel : MonoBehaviour
{
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private Vector2 spacingGroup;
    [SerializeField] private Vector2 padding;
    [SerializeField] private Vector2Int countElement;
    [SerializeField] private Vector2[] sizeElement;
    [SerializeField] private float speedMove;
    [SerializeField] private bool freezeX;
    [SerializeField] private bool freezeY;
    [SerializeField] private DragPanel dragPanelParrent;
    private Vector3 lastMousePosition;
    private bool isDrag;
    private bool isMove;
    private Vector2 maxDrag;
    private Vector2 minDrag;
    private Vector2 coefficientResolutions;
    private Vector2 lastOffset;
    private Vector2 freezePosition;


    private void Start()
    {
        coefficientResolutions.x = Setting.ReferenceResolutions.x / Camera.main.pixelWidth;
        coefficientResolutions.y = Setting.ReferenceResolutions.y / Camera.main.pixelHeight;
        Vector2 lengthElements= Vector2.zero;
        for (int i = 0; i < sizeElement.Length; i++)
        {
            lengthElements.x += sizeElement[i].x* countElement.x+ spacingGroup.x * (countElement.x + 1);
            lengthElements.y += sizeElement[i].y * countElement.y+ spacingGroup.y * (countElement.y + 1);
        }

        SetMinMaxDrag(lengthElements);

        if (freezeY && freezeX)
        {
            enabled = false;
        }
        if (freezeY || freezeX)
        {
            freezePosition = rectTransform.anchoredPosition;
        }
    }
    private void SetMinMaxDrag(Vector2 lengthElements)
    {
        if (Setting.ReferenceResolutions.x > lengthElements.x + padding.x)
        {
            minDrag.x = rectTransform.anchoredPosition.x +  padding.x; 
            maxDrag.x = rectTransform.anchoredPosition.x + Setting.ReferenceResolutions.x - lengthElements.x - padding.x;
        }
        else
        {
            minDrag.x = rectTransform.anchoredPosition.x + Setting.ReferenceResolutions.x - lengthElements.x - padding.x;
            maxDrag.x = rectTransform.anchoredPosition.x +  padding.x; 
        }
        if (Setting.ReferenceResolutions.y > lengthElements.y + padding.y)
        {
            minDrag.y = rectTransform.anchoredPosition.y + lengthElements.y + padding.y - Setting.ReferenceResolutions.y;
            maxDrag.y = rectTransform.anchoredPosition.y - padding.y; 
        }
        else
        {
            minDrag.y = rectTransform.anchoredPosition.y - padding.y;
            maxDrag.y = rectTransform.anchoredPosition.y + lengthElements.y + padding.y - Setting.ReferenceResolutions.y; 
        }
    }
    private void OnMouseDown()
    {
        lastMousePosition = Input.mousePosition;
        isDrag = true;
        isMove = true;
        if (dragPanelParrent != null)
        {
            dragPanelParrent.SetIsDrag(isDrag);
            dragPanelParrent.SetIsMove(isMove);
        }

    }
    private void OnMouseDrag()
    {
        if (!isDrag)
        {
            return;
        }
        MovePanel(lastMousePosition);
        if (dragPanelParrent!=null)
        {
            dragPanelParrent.MovePanel(lastMousePosition);
        }
        this.lastMousePosition = Input.mousePosition;
    }
    public void MovePanel(Vector3 lastMousePosition)
    {
        var offset = (Vector2)(Input.mousePosition - lastMousePosition);
        offset.x *= coefficientResolutions.x;
        offset.y *= coefficientResolutions.y;
        offset += lastOffset;

        var anchoredPosition = rectTransform.anchoredPosition;

        var target = anchoredPosition + offset;

        anchoredPosition = Vector2.Lerp(rectTransform.anchoredPosition, target, Time.deltaTime * speedMove);
        lastOffset = target - anchoredPosition;

        if (freezeX)
        {
            anchoredPosition.x = freezePosition.x;
        }
        if (freezeY)
        {
            anchoredPosition.y = freezePosition.y;
        }
        rectTransform.anchoredPosition = anchoredPosition;

    }
    private void OnMouseUp()
    {
        isDrag = false;
        if (dragPanelParrent != null)
        {
            dragPanelParrent.SetIsDrag(isDrag);
        }
    }
    public void SetIsDrag(bool isDrag)
    {
        this.isDrag = isDrag;
    }
    public void SetIsMove(bool isMove)
    {
        this.isMove = isMove;
    }
    private void Update()
    {
        if (freezeY && freezeX)
        {
            enabled = false;
            return;
        }
        if (!isMove)
        {
            return;
        }
        if (isDrag)
        {
            return;
        }
        var anchoredPosition = rectTransform.anchoredPosition;

        var nextPosition = anchoredPosition.ClampVector2(minDrag, maxDrag);
        anchoredPosition = Vector2.Lerp(anchoredPosition, nextPosition, Time.deltaTime* speedMove);
        rectTransform.anchoredPosition = anchoredPosition;
        if ((anchoredPosition-nextPosition).magnitude<0.01)
        {
            anchoredPosition = nextPosition;
            isMove = false;
        }
        rectTransform.anchoredPosition = anchoredPosition;
    }
}
