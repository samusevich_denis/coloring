﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeImageButton : MonoBehaviour
{
 
    [SerializeField] Sprite spritePassive;
    [SerializeField] Sprite spriteActive;
    private bool isActive;
    private Image image;


    // Start is called before the first frame update
    private void Awake()
    {
        image = GetComponent<Image>();
    }
    void Start()
    {

        ChangeSprite();
    }

    public void ChangeSprite()
    {
        image.sprite = isActive ? spriteActive : spritePassive;
        isActive = !isActive;
    }

}
