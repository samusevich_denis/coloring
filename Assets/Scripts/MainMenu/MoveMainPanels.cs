﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Panel
{
    Exsplore,
    MyPaints,
    Gifts,
    Info,
}
public class MoveMainPanels : MonoBehaviour
{

    [SerializeField] private ChangeImageButton[] buttonsDownPanel;
    [SerializeField] private RectTransform[] panels;
    [SerializeField] private float speedMove;
    private Panel currentPanel;
    private Panel nextPanel;
    private bool isMove;
    private Vector2 leftPosition;
    private Vector2 centerPosition;
    private Vector2 rightPosition;



    void Start()
    {
        currentPanel = Panel.Exsplore;
        leftPosition = new Vector2(-Setting.ReferenceResolutions.x, 0f);
        centerPosition = Vector2.zero;
        rightPosition = new Vector2(+Setting.ReferenceResolutions.x, 0f);

        for (int i = 0; i < panels.Length; i++)
        {
            if (i == (int) Panel.Exsplore)
            {
                panels[i].anchoredPosition = centerPosition;
                buttonsDownPanel[i].ChangeSprite();
                continue;
            }
            panels[i].anchoredPosition = leftPosition;
        }


    }

    public void MoveNextPanel(int nextPanel)
    {
        if (isMove || nextPanel == (int)currentPanel)
        {
            return;
        }
        buttonsDownPanel[nextPanel].ChangeSprite();
        buttonsDownPanel[(int)currentPanel].ChangeSprite();
        this.nextPanel =(Panel) nextPanel;
        isMove = true;
        panels[nextPanel].anchoredPosition = leftPosition;


    }
    // Update is called once per frame
    void Update()
    {
        if (!isMove)
        {
            return;
        }

        var anchoredCurrentPosition = panels[(int)currentPanel].anchoredPosition;
        var anchoredNextPosition = panels[(int)nextPanel].anchoredPosition;
        anchoredCurrentPosition = Vector2.Lerp(anchoredCurrentPosition, rightPosition, Time.deltaTime * speedMove);
        anchoredNextPosition = Vector2.Lerp(anchoredNextPosition, centerPosition, Time.deltaTime * speedMove);

        panels[(int)currentPanel].anchoredPosition = anchoredCurrentPosition;
        panels[(int)nextPanel].anchoredPosition = anchoredNextPosition;

        if ((anchoredCurrentPosition - rightPosition).magnitude < 0.5)
        {
            panels[(int)currentPanel].anchoredPosition = rightPosition;
            panels[(int)nextPanel].anchoredPosition = centerPosition;
            isMove = false;
            currentPanel = nextPanel;
        }
    }
}
