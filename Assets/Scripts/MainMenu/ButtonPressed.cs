﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPressed : MonoBehaviour
{
    private float time = 0.5f;
    public bool isMouseDrag;
    private Coroutine coroutineMouseDownTimer;

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            coroutineMouseDownTimer = StartCoroutine(MouseDownTimer(time));
        }

    }
    private IEnumerator MouseDownTimer(float time)
    {
        isMouseDrag = false;
        yield return new WaitForSeconds(time);
        isMouseDrag = true;
        coroutineMouseDownTimer = null;
    }
}
