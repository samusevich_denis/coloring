﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataImage 
{
    public int id;
    public string nameImage;
    public Sprite sprite;
    public DataFigure[] dataFigure;

    public DataImage(int id, string nameImage, Sprite sprite, DataFigure[] dataFigure)
    {
        this.id = id;
        this.nameImage = nameImage;
        this.sprite = sprite;
        this.dataFigure = dataFigure;
    }
    public DataImage(int id, string nameImage, DataFigure[] dataFigure)
    {
        this.id = id;
        this.nameImage = nameImage;
        this.dataFigure = dataFigure;
    }
}
