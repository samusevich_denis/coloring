﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "DataGame", menuName = "ObjectData/DataGame", order = 0)]
public class DataGame : ScriptableObject
{
    public DataSection[] dataSections;
#if UNITY_EDITOR
    public static void CreateAsset<T>(T obj, int index) where T : ScriptableObject
    {
        var asset = obj;
        string assetPathAndName = "Assets/DataGame/"+typeof(T).Name+ index.ToString("D3") + ".asset";
        AssetDatabase.CreateAsset(asset, assetPathAndName);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public static T LoadAsset<T>(int index) where T : ScriptableObject
    {
        T asset;
        string assetPathAndName = "Assets/DataGame/" + typeof(T).Name + index.ToString("D3") + ".asset";
        asset = AssetDatabase.LoadAssetAtPath<T>(assetPathAndName);
        return asset;
    }

    public static void DeleteAsset<T>(int index) where T : ScriptableObject
    {
        string assetPathAndName = "Assets/DataGame/" + typeof(T).Name + index.ToString("D3") + ".asset";
        AssetDatabase.DeleteAsset(assetPathAndName);
        AssetDatabase.Refresh();
    }
#endif
}
