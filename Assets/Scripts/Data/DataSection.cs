﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataSection", menuName = "ObjectData/DataSection", order = 1)]
public class DataSection : ScriptableObject
{
    public int idSection;
    public string nameSection;
    public DataPicture[] dataPictures;
}
