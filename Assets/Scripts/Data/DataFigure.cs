﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataFigure
{
    public int id;
    public bool isUnpainted;
    public int sortingLayer;
    public int requiredСolorId;
    public Vector2[] contour;
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uv;

    public DataFigure(int id, bool isUnpainted,int sortingLayer, int requiredСolorId, Vector2[] contour, Vector3[] vertices, int[] triangles, Vector2[] uv)
    {
        this.id = id;
        this.isUnpainted = isUnpainted;
        this.sortingLayer = sortingLayer;
        this.requiredСolorId = requiredСolorId;
        this.contour = contour;
        this.vertices = vertices;
        this.triangles = triangles;
        this.uv = uv;
    }
}
