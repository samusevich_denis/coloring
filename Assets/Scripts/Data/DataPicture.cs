﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DataPicture : ScriptableObject
{
    public DataImage dataImage;
    public Color[] palette;
    public Dictionary<int,List<int>> figureIndicesSortedByColor;
    public Dictionary<int, Color> colorRules;

    public void UpdateDataPicture()
    {
        colorRules = GetColorRules(palette);
        figureIndicesSortedByColor = GetFigureIndicesSortedByColor();
    }

    private Dictionary<int, Color> GetColorRules(Color[] colors)
    {
        var dataFigures = dataImage.dataFigure;
        Dictionary<int, Color> colorRules = new Dictionary<int, Color>();
        for (int i = 0; i < dataFigures.Length; i++)
        {
            var colorId = dataFigures[i].requiredСolorId;
            var color = colors[dataFigures[i].requiredСolorId];
            if (!colorRules.ContainsKey(colorId))
            {
                colorRules.Add(colorId, color);
            }
        }
        Debug.Log($"Created {colorRules.Count} colors");
        return colorRules;
    }

    private Dictionary<int, List<int>> GetFigureIndicesSortedByColor()
    {
        Dictionary<int, List<int>> figureIndicesSortedByColor = new Dictionary<int, List<int>>();
        var dataFigures = dataImage.dataFigure;
        var keys = colorRules.Keys;
        foreach (var key in keys)
        {
            figureIndicesSortedByColor.Add(key, new List<int>());
            for (int i = 0; i < dataFigures.Length; i++)
            {
                if (key == dataFigures[i].requiredСolorId)
                {
                    figureIndicesSortedByColor[key].Add(i);
                }
            }
            if (figureIndicesSortedByColor[key].Count == 0)
            {
                Debug.LogError($"key№{key} has no indexes");
            }
        }
        return figureIndicesSortedByColor;
    }
}
