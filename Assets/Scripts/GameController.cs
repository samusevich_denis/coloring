﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController:MonoBehaviour
{
    public static GameController controller;
    public DataPicture currentDataPicture;
    [SerializeField] DataGame dataGame;
    public void Awake()
    {
        if (controller==null)
        {
            controller = this;
        }
        DontDestroyOnLoad(this);
        if (FindObjectsOfType(GetType()).Length > 1)
            Destroy(gameObject);
        
    }


    public void LoadPicture(int section, int image)
    {
        currentDataPicture = dataGame.dataSections[section].dataPictures[image];
        SceneManager.LoadScene(1);
    }

    public void LoadMainMenu()
    {
        currentDataPicture = null;
        SceneManager.LoadScene(0);
    }
}
