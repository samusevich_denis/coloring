﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePictureSelect : MonoBehaviour
{
    private DataPicture dataPicture;

    public Material selectableMaterial;
    public Material standartMaterial;
    private GameFigure[] gameFigures;
    private int selectableCurrentColorId;
    
    // Start is called before the first frame update
    void Start()
    {
        selectableCurrentColorId = -1;
        dataPicture = GameController.controller.currentDataPicture;
        gameFigures = CreatorGameFigure.CreateGameFigure(dataPicture,transform, standartMaterial);

    }


    public Color GetColor(int colorId)
    {
        return dataPicture.colorRules[colorId];
    }
    public bool SelectFigures(int colorId)
    {
        if (colorId == selectableCurrentColorId)
        {
            return true;
        }
        return false;
    }

    public void SelectColor(int colorId)
    {
        SetMaterial(standartMaterial);
        selectableCurrentColorId = colorId;
        SetMaterial(selectableMaterial);
    }
    private void SetMaterial(Material material)
    {
        if (selectableCurrentColorId<0)
        {
            return;
        }
        Debug.Log(selectableCurrentColorId);
        var indicesFigures = dataPicture.figureIndicesSortedByColor[selectableCurrentColorId];
        for (int i = 0; i < indicesFigures.Count; i++)
        {
            gameFigures[indicesFigures[i]].SetMaterial(material);
        }
    }
}
