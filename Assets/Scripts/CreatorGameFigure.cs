﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatorGameFigure 
{
    public static GameFigure[] CreateGameFigure(DataPicture dataPicture, Transform transformParent, Material standartMaterial)
    {
        dataPicture.UpdateDataPicture();

        var figures = dataPicture.dataImage.dataFigure;




        for (int i = 0; i < figures.Length; i++)
        {
            if (figures[i].isUnpainted && (figures[i].requiredСolorId !=0 && figures[i].requiredСolorId!=7))
            {
                figures[i].isUnpainted = false;
            }
        }




        GameFigure[] gameFigures = new GameFigure[figures.Length];
        for (int i = 0; i < figures.Length; i++)
        {
            var currentFigure = new GameObject($"Figure №{i}", typeof(MeshFilter), typeof(MeshRenderer), typeof(PolygonCollider2D), typeof(GameFigure), typeof(ColorFigure));
            currentFigure.transform.position =new Vector3(0,0, -figures[i].sortingLayer);
            currentFigure.transform.parent = transformParent;
            var meshRenderer = currentFigure.GetComponent<MeshRenderer>();
            var meshFilter = currentFigure.GetComponent<MeshFilter>();
            var polygonCollider2D = currentFigure.GetComponent<PolygonCollider2D>();
            gameFigures[i] = currentFigure.GetComponent<GameFigure>();
            gameFigures[i].SetDataFigure(figures[i]);

            meshRenderer.material = standartMaterial;
            MaterialPropertyBlock props = new MaterialPropertyBlock();
            Color color = Color.white;
            if (figures[i].isUnpainted)
            {
                color = dataPicture.colorRules[figures[i].requiredСolorId];
            }
            props.SetColor("_Color", color);
            meshRenderer.SetPropertyBlock(props);

            Mesh mesh = new Mesh();
            mesh.vertices = figures[i].vertices;
            mesh.triangles = figures[i].triangles;
            mesh.uv = figures[i].uv;

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            meshFilter.sharedMesh = mesh;

            polygonCollider2D.points = figures[i].contour;
        }
        return gameFigures;
    }
}
