﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FigureCreator : EditorWindow
{
    private static List<Line> lines = new List<Line>();
    private static List<Figure> figures = new List<Figure>();
    private static float minLine;

    [MenuItem("Tools/SVGSprite/ResetLineAndFigure")]
    private static void ResetLineAndFigure()
    {
        lines = new List<Line>();
        figures = new List<Figure>();
    }
    [MenuItem("Tools/SVGSprite/CreatуLineFromSVG")]
    private static void CriateLineInSprite()
    {
        var spriteObject = Selection.activeGameObject;
        if (spriteObject == null)
        {
            Debug.Log("object null");
            return;
        }

        var spriteRenderer = spriteObject.GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            Debug.Log("SpriteRenderer null");
            return;
        }
        var sprite = spriteRenderer.sprite;
        var triangles = sprite.triangles;
        var vertices = sprite.vertices;
        var objParent = new GameObject("parent");
        lines = new List<Line>(triangles.Length);
        minLine = float.MaxValue;
        for (int i = 0; i < triangles.Length; i += 3)
        {
            for (int j = 0; j < 3; j++)
            {
                int index = j == 2 ? -1 : j;
                Line newLine = new Line(vertices[triangles[i + j]], vertices[triangles[i + index + 1]]);
                if (minLine > newLine.magnitude)
                {
                    if (newLine.magnitude == 0)
                    {
                        //lines.Add(newLine);
                        //Debug.Log($"{newLine.magnitude} -------- {i + j}");
                        break;
                    }
                    minLine = newLine.magnitude;
                    //Debug.Log($"{minLine} -------- {i + j}");
                }
                lines.Add(newLine);
            }
        }

        //LinQ
        for (int i = 0; i < lines.Count; i++)
        {
            List<int> indexesToDelete = new List<int>();
            indexesToDelete.Add(i);
            for (int j = i + 1; j < lines.Count; j++)
            {
                if (Line.LineComparison(lines[i], lines[j], minLine))
                {
                    indexesToDelete.Add(j);
                }
            }
            if (indexesToDelete.Count > 1)
            {
                for (int j = 0; j < indexesToDelete.Count; j++)
                {
                    lines.RemoveAt(indexesToDelete[j] - j);
                }
                i--;
            }
        }
        for (int i = 0; i < lines.Count; i++)
        {

            var obj = new GameObject(i.ToString());
            obj.transform.position = lines[i].pointOne;
            var edgeCollider = obj.AddComponent<EdgeCollider2D>();
            Vector2[] points = new Vector2[2];
            points[0] = Vector2.zero;
            points[1] = lines[i].pointSecond - lines[i].pointOne;
            edgeCollider.points = points;
            obj.transform.parent = objParent.transform;
        }

    }
    [MenuItem("Tools/SVGSprite/CreateFiguresFromLine")]
    private static void SortAndCreateFigure()
    {
        while (lines.Count != 0)
        {
            Figure figure = new Figure(lines[0], minLine);
            lines.RemoveAt(0);
            for (int j = 0; j < lines.Count; j++)
            {
                if (figure.СoincidenceOfPoints(lines[j], minLine))
                {
                    figure.AddLine(lines[j]);
                    lines.RemoveAt(j);
                    j = -1;
                }
                if (figure.IsFigureCreated)
                {
                    break;
                }
            }
            if (!figure.IsFigureCreated)
            {
                figure.DrawFigure();
                Debug.LogError("wrong figure");
            }
            figure.Optimization(0.001f);
            figures.Add(figure);
        }
        Debug.Log(figures.Count);
    }

    [MenuItem("Tools/SVGSprite/CreateGameobjectFromFigure")]
    private static void CreateGameobjectFromFigure()
    {
        for (int i = 0; i < figures.Count; i++)
        {
            var obj = new GameObject();
            obj.transform.position = Vector3.zero;
            obj.AddComponent<MeshRenderer>();
            var meshFilter = obj.AddComponent<MeshFilter>();
            var mesh = MeshCreator.GetMeshFromFigure(figures[i]);
            meshFilter.sharedMesh = mesh;
            var rigidbody = obj.AddComponent<Rigidbody2D>();
            rigidbody.isKinematic = true;
            var polygonCollider = obj.AddComponent<PolygonCollider2D>();
            Vector2[] verticesCollider = new Vector2[figures[i].LinesFigure.Count];
            for (int j = 0; j < verticesCollider.Length; j++)
            {
                verticesCollider[j] = figures[i].LinesFigure[j].pointOne;
            }
            polygonCollider.points = verticesCollider;
        }

        //Sprite dont work
        //for (int i = 0; i < figures.Count; i++)
        //{
        //    var obj = new GameObject();
        //    obj.transform.position = Vector3.zero;
        //    var spriteRenderer = obj.AddComponent<SpriteRenderer>();
        //    var texture2D = Texture2D.whiteTexture;
        //    var sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.zero, 100);
        //    spriteRenderer.sprite = sprite;
        //    //var sprite = Sprite.Create(Texture2D.whiteTexture, Rect.zero, Vector2.zero);
        //    MeshCreator.GetSpriteGeometryFromFigure(figures[i], out Vector2[] vertices, out ushort[] triangles);

        //    sprite.OverrideGeometry(vertices, triangles);

        //    var rigidbody = obj.AddComponent<Rigidbody2D>();
        //    rigidbody.isKinematic = true;
        //    var polygonCollider = obj.AddComponent<PolygonCollider2D>();
        //    Vector2[] verticesCollider = new Vector2[figures[i].LinesFigure.Count];
        //    for (int j = 0; j < verticesCollider.Length; j++)
        //    {
        //        verticesCollider[j] = figures[i].LinesFigure[j].pointOne;
        //    }
        //    polygonCollider.points = verticesCollider;
        //}
    }
}
