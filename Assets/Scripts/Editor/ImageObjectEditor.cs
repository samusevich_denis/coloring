﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class ImageObjectEditor : EditorWindow
{
    public DataImage dataImage;
    public Material materialImage;
    public float minLengthLine;
    public Color[] palette;
    private GameObject currentGameObject;

    private GameObject currentGameObjectFigure;
    private GameObject[] allGameObjectFigure;
    private Vector2 scrolPosition = Vector2.zero;


    [MenuItem("Tools/Image Object Editor")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(ImageObjectEditor)).Show();
    }
    private void Awake()
    {
        palette = new Color[1];
        palette[0] = Color.white;
    }

    private void OnGUI()
    {
        scrolPosition = GUILayout.BeginScrollView(scrolPosition, GUIStyle.none);
        SerializedObject serializedObject = new SerializedObject(this);
        var serializedPropertyDataImage = serializedObject.FindProperty("dataImage");
        var serializedPropertyPalette = serializedObject.FindProperty("palette");
        var serializedId = serializedPropertyDataImage.FindPropertyRelative("id");
        var serializedNameImage = serializedPropertyDataImage.FindPropertyRelative("nameImage");
        var serializedSprite = serializedPropertyDataImage.FindPropertyRelative("sprite");
        var serializedDataFigure = serializedPropertyDataImage.FindPropertyRelative("dataFigure");
        var serializedPropertyMaterialImage = serializedObject.FindProperty("materialImage");
        var serializedPropertyMinLengthLine = serializedObject.FindProperty("minLengthLine");

        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical(GUILayout.Width(300));

        GUILayout.Label("Setting");
        EditorGUILayout.PropertyField(serializedPropertyMinLengthLine, true);
        EditorGUILayout.PropertyField(serializedPropertyMaterialImage, true);


        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button($"Show all figures"))
        {
            ShowAllFigures();
        }
        if (GUILayout.Button($"Paint over all figures and show"))
        {
            PaintAndShowAllFigures();
        }
        if (GUILayout.Button($"Remove all figures"))
        {
            RemoveAllFigures();
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button($"Save DataImage"))
        {
            SaveDataImage();
        }
        if (GUILayout.Button($"Read again DataImage from object"))
        {
            ReadDataImageFromObject();
        }
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();

        GUILayout.BeginVertical(GUILayout.Width(300));

        GUILayout.Label("Palette");
        for (int i = 0; i < serializedPropertyPalette.arraySize; i++)
        {
            EditorGUILayout.PropertyField(serializedPropertyPalette.GetArrayElementAtIndex(i), true);
        }
        GUILayout.BeginHorizontal();
        if (GUILayout.Button($"Add color to palette"))
        {
            AddСolorToPalette();
        }

        GUILayout.EndHorizontal();
        GUILayout.EndVertical();

        GUILayout.BeginVertical(GUILayout.Width(300));
        GUILayout.Label("DataImage");
        EditorGUILayout.PropertyField(serializedId, true);
        EditorGUILayout.PropertyField(serializedNameImage, true);
        EditorGUILayout.PropertyField(serializedSprite, true);


        GUILayout.Label($"Figure Object Editor");

        for (int i = 0; i < serializedDataFigure.arraySize; i++)
        {
            GUILayout.Space(10);
            GUILayout.Label($"Figure №{i}");
            EditorGUILayout.PropertyField(serializedDataFigure.GetArrayElementAtIndex(i), true);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button($"Show Figure №{i} in scene"))
            {
                ShowFigureInScene(i);
            }
            if (GUILayout.Button($"Delete Figure №{i} "))
            {
                DeleteFigureInScene();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();
        serializedObject.ApplyModifiedProperties();
    }

    private void AddСolorToPalette()
    {
        var newPalette = new Color[palette.Length+1];
        for (int i = 0; i < palette.Length; i++)
        {
            newPalette[i] = palette[i];
        }
        newPalette[palette.Length] = Color.white;
        palette = newPalette;
    }
    private void SaveDataImage()
    {
        var dataPicture = CreateInstance<DataPicture>();
        dataPicture.dataImage = dataImage;
        dataPicture.palette = palette;
        DataGame.CreateAsset<DataPicture>(dataPicture, dataImage.id);
    }
    private void PaintAndShowAllFigures()
    {
        RemoveAllFigures();
        allGameObjectFigure = new GameObject[dataImage.dataFigure.Length];
        for (int i = 0; i < allGameObjectFigure.Length; i++)
        {
            allGameObjectFigure[i] = CreateFigureInScene(i);
        }
    }
    private void RemoveAllFigures()
    {
        if (allGameObjectFigure != null)
        {
            for (int i = 0; i < allGameObjectFigure.Length; i++)
            {
                DestroyImmediate(allGameObjectFigure[i]);
            }
        }
    }

    private void ShowAllFigures()
    {
        RemoveAllFigures();
        allGameObjectFigure = new GameObject[dataImage.dataFigure.Length];
        for (int i = 0; i < allGameObjectFigure.Length; i++)
        {
            allGameObjectFigure[i] = CreateFigureInScene(i, Color.white);
        }
    }
    private void DeleteFigureInScene()
    {
        if (currentGameObjectFigure != null)
        {
            GameObject.DestroyImmediate(currentGameObjectFigure);
            currentGameObjectFigure = null;
        }
    }
    private void ShowFigureInScene(int indexFigure)
    {
        DeleteFigureInScene();
        currentGameObjectFigure = CreateFigureInScene(indexFigure);
    }
    private GameObject CreateFigureInScene(int indexFigure)
    {
        int indexColor = dataImage.dataFigure[indexFigure].requiredСolorId;
        if (indexColor > palette.Length-1)
        {
            Debug.LogError("color is not in the palette");
        }
        return CreateFigureInScene(indexFigure, palette[indexColor]);
    }
    private GameObject CreateFigureInScene(int indexFigure, Color color) 
    {
        var currentFigure = new GameObject($"Figure №{indexFigure}",typeof(MeshFilter),typeof(MeshRenderer),typeof(PolygonCollider2D));
        currentFigure.transform.position = Vector3.zero;

        var dataFigure = dataImage.dataFigure[indexFigure];

        var meshRenderer = currentFigure.GetComponent<MeshRenderer>();
        var meshFilter = currentFigure.GetComponent<MeshFilter>();
        var polygonCollider2D = currentFigure.GetComponent<PolygonCollider2D>();

        meshRenderer.material = materialImage;
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        props.SetColor("_Color", color);
        meshRenderer.SetPropertyBlock(props);

        Mesh mesh = new Mesh();
        mesh.vertices = dataFigure.vertices;
        mesh.triangles = dataFigure.triangles;
        mesh.uv = dataFigure.uv;

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        meshFilter.sharedMesh = mesh;
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            Debug.Log(mesh.vertices[i]);
        }
        polygonCollider2D.points = dataFigure.contour;
        return currentFigure;
    }


    private void ReadDataImageFromObject()
    {
        currentGameObject = Selection.activeGameObject;
        if (currentGameObject == null)
        {
            Debug.LogWarning("GameObject not selected");
            return;
        }

        var spriteRenderer = currentGameObject.GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            Debug.LogWarning("SpriteRenderer not found");
            return;
        }

        var lines = DataImageCreator.CreateLineFromSprite(spriteRenderer);
        var figures = DataImageCreator.SortAndCreateFigureFromLine(lines, minLengthLine);
        dataImage = DataImageCreator.CreateDataFromFigure(figures);
    }

}
