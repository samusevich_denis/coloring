﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataImageCreator 
{
    private static float minLine;
    public static List<Line> CreateLineFromSprite(SpriteRenderer spriteRenderer)
    {
        var sprite = spriteRenderer.sprite;
        var triangles = sprite.triangles;
        var vertices = sprite.vertices;
        List<Line> lines = new List<Line>(triangles.Length);
        minLine = float.MaxValue;
        for (int i = 0; i < triangles.Length; i += 3)
        {
            for (int j = 0; j < 3; j++)
            {
                int index = j == 2 ? -1 : j;
                Line newLine = new Line(vertices[triangles[i + j]], vertices[triangles[i + index + 1]]);
                if (minLine > newLine.magnitude)
                {
                    if (newLine.magnitude == 0)
                    {
                        break;
                    }
                    minLine = newLine.magnitude;
                }
                lines.Add(newLine);
            }
        }

        //LinQ
        for (int i = 0; i < lines.Count; i++)
        {
            List<int> indexesToDelete = new List<int>();
            indexesToDelete.Add(i);
            for (int j = i + 1; j < lines.Count; j++)
            {
                if (Line.LineComparison(lines[i], lines[j], minLine))
                {
                    indexesToDelete.Add(j);
                }
            }
            if (indexesToDelete.Count > 1)
            {
                for (int j = 0; j < indexesToDelete.Count; j++)
                {
                    lines.RemoveAt(indexesToDelete[j] - j);
                }
                i--;
            }
        }
        return lines;
    }
    public static List<Figure> SortAndCreateFigureFromLine(List<Line> lines, float lengthMinLine)
    {
        List<Figure> figures = new List<Figure>();
        while (lines.Count != 0)
        {
            Figure figure = new Figure(lines[0], minLine);
            lines.RemoveAt(0);
            for (int j = 0; j < lines.Count; j++)
            {
                if (figure.СoincidenceOfPoints(lines[j], minLine))
                {
                    figure.AddLine(lines[j]);
                    lines.RemoveAt(j);
                    j = -1;
                }
                if (figure.IsFigureCreated)
                {
                    break;
                }
            }
            if (figures.Count==143)
            {
                Debug.Log(figures);
            }
            if (!figure.IsFigureCreated)
            {
                figure.DrawFigure();
                Debug.LogError("wrong figure");
            }
            figure.Optimization(lengthMinLine);
            figures.Add(figure);
        }
        Debug.Log($"{figures.Count} figures created");
        return figures;
    }


    public static DataImage CreateDataFromFigure(List<Figure> figures)
    {
        DataFigure[] dataFigures = new DataFigure[figures.Count];
        for (int i = 0; i < figures.Count; i++)
        {
            var mesh = MeshCreator.GetMeshFromFigure(figures[i]);
            dataFigures[i] = new DataFigure(i, false, 0, 0, figures[i].Contour, mesh.vertices, mesh.triangles, mesh.uv);

        }
        return new DataImage(0,string.Empty, dataFigures);
    }


}
