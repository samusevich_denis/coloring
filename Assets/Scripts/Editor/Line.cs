﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Line 
{
    public Vector2 pointOne;
    public Vector2 pointSecond;
    public float magnitude 
    {
        get 
        {
            return (pointSecond - pointOne).magnitude;
        }
    }

    public Line(Vector2 pointOne, Vector2 pointSecond)
    {
        this.pointOne = pointOne;
        this.pointSecond = pointSecond;
    }

    public static bool LineComparison(Line lineOne, Line lineSecond, float degreeOfError)
    {
        return (lineOne.pointOne.pointComparisons(lineSecond.pointOne, degreeOfError) && lineOne.pointSecond.pointComparisons(lineSecond.pointSecond,degreeOfError)) ||
            (lineOne.pointSecond.pointComparisons(lineSecond.pointOne, degreeOfError) && lineOne.pointOne.pointComparisons(lineSecond.pointSecond, degreeOfError));
    }
    //public bool СoincidenceOfPoints(Line line, float degreeOfError)
    //{
    //    return pointOne.pointComparisons(line.pointOne, degreeOfError) || pointSecond.pointComparisons(line.pointSecond, degreeOfError) ||
    //        pointSecond.pointComparisons(line.pointOne, degreeOfError) || pointOne.pointComparisons(line.pointSecond, degreeOfError);
    //}
}
