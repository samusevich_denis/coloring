﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure
{
    public List<Line> LinesFigure { get; private set; }
    public Line LastLine { get { return LinesFigure[LinesFigure.Count - 1]; } }
    public Vector2[] Contour { get; private set; }
    public bool IsFigureCreated { get; private set; }
    private float degreeOfErrorPoints;
    public Figure(Line line, float degreeOfErrorPoints)
    {
        LinesFigure = new List<Line>();
        IsFigureCreated = false;
        this.degreeOfErrorPoints = degreeOfErrorPoints;
        LinesFigure.Add(line);
    }
    public void AddLine(Line line)
    {
        line = SortLine(line);
        LinesFigure.Add(line);
        if (line.pointSecond.pointComparisons(LinesFigure[0].pointOne, degreeOfErrorPoints))
        {
            IsFigureCreated = true;
            UpdateContour();
        }
    }

    public bool СoincidenceOfPoints(Line line, float degreeOfError)
    {
        return LastLine.pointSecond.pointComparisons(line.pointSecond, degreeOfError) || LastLine.pointSecond.pointComparisons(line.pointOne, degreeOfError);
    }
    private Line SortLine(Line line)
    {
        if (LastLine.pointSecond.pointComparisons(line.pointOne, degreeOfErrorPoints))
        {
            return line;
        }
        else if (LastLine.pointSecond.pointComparisons(line.pointSecond, degreeOfErrorPoints))
        {
            var posSecond = line.pointOne;
            line.pointOne = line.pointSecond;
            line.pointSecond = posSecond;
            return line;
        }
        else
        {
            Debug.LogError("Wrong line");
            return line;
        }
    }
    public Vector2 GetCenterFigure()
    {
        Vector2 pointCenter = Vector2.zero;
        for (int i = 0; i < LinesFigure.Count; i++)
        {
            pointCenter += LinesFigure[i].pointOne;
        }
        pointCenter/= LinesFigure.Count;
        return pointCenter;
    }
    public void DrawFigure()
    {
        var objParent = new GameObject("parent");
        for (int i = 0; i < LinesFigure.Count; i++)
        {

            var obj = new GameObject(i.ToString());
            obj.transform.position = LinesFigure[i].pointOne;
            var edgeCollider = obj.AddComponent<EdgeCollider2D>();
            Vector2[] points = new Vector2[2];
            points[0] = Vector2.zero;
            points[1] = LinesFigure[i].pointSecond - LinesFigure[i].pointOne;
            edgeCollider.points = points;
            obj.transform.parent = objParent.transform;
        }
    }

    public void Optimization(float minLenghtLine)
    {
        for (int i = 0; i < LinesFigure.Count; i++)
        {
            if (LinesFigure[i].magnitude < minLenghtLine)
            {
                int previousIndex = i == 0 ? LinesFigure.Count - 1 : i-1;
                int nextIndex = i == LinesFigure.Count - 1 ? 0 : i + 1;
                var previousLine = new Line(LinesFigure[previousIndex].pointOne, LinesFigure[nextIndex].pointOne);
                LinesFigure[previousIndex] = previousLine;
                LinesFigure.RemoveAt(i);
                i--;
            }

        }
        for (int i = 0; i < LinesFigure.Count; i++)
        {
            int previousIndex = i == 0 ? LinesFigure.Count - 1 : i - 1;
            int nextIndex = i == LinesFigure.Count - 1 ? 0 : i + 1;
            if ((Mathf.Abs(LinesFigure[previousIndex].pointSecond.x - LinesFigure[i].pointSecond.x)< minLenghtLine 
                && Mathf.Abs(LinesFigure[i].pointSecond.x - LinesFigure[nextIndex].pointSecond.x)<minLenghtLine)
                || (Mathf.Abs(LinesFigure[previousIndex].pointSecond.y - LinesFigure[i].pointSecond.y) < minLenghtLine 
                && Mathf.Abs(LinesFigure[i].pointSecond.y - LinesFigure[nextIndex].pointSecond.y) < minLenghtLine))
            {
                
                var line = new Line(LinesFigure[previousIndex].pointSecond, LinesFigure[nextIndex].pointSecond);
                LinesFigure[i] = line;
                Debug.Log(line);
                LinesFigure.RemoveAt(nextIndex);
                i--;
            }

        }
        UpdateContour();
    }
    private void UpdateContour()
    {
        if (!IsFigureCreated)
        {
            Debug.LogError("wrong contour");
            return;
        }
        Contour = new Vector2[LinesFigure.Count];
        for (int i = 0; i < Contour.Length; i++)
        {
            Contour[i] = LinesFigure[i].pointOne;
        }
    }

}
