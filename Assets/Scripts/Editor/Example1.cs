﻿using UnityEditor;
using UnityEngine;

static class Example1
{
    [MenuItem("Edit/Reset Selected Objects Position (No Undo)")]
    static void ResetPosition()
    {
        // this action will not be undoable
        foreach (var go in Selection.gameObjects)
            go.transform.localPosition = Vector3.zero;
    }
}
