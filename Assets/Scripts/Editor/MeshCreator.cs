﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCreator
{

    public static Mesh GetMeshFromFigure(Figure figure)
    {
        List<Vector3> listPoints = new List<Vector3>();
        for (int i = 0; i < figure.LinesFigure.Count; i++)
        {
            listPoints.Add(figure.LinesFigure[i].pointOne);
        }

        Triangulation.GetResult(listPoints, true, -Vector3.forward, out Vector3[] verticles, out int[] triangles, out Vector2[] uvcoords);
        var isTrigger = false;
        if (verticles.Length == 0)
        {
            Triangulation.GetResult(listPoints, false, -Vector3.forward, out verticles, out  triangles, out  uvcoords);
            isTrigger = true;
        }
        for (int i = 0; i < triangles.Length; i += 3)
        {
            if (isTrigger)
            {
                break;
            }
            int ver = triangles[i + 1];
            triangles[i + 1] = triangles[i + 2];
            triangles[i + 2] = ver;
            
        }
        Mesh mesh = new Mesh();
        mesh.vertices = verticles;
        mesh.triangles = triangles;
        mesh.uv = uvcoords;


        return mesh;
    }
    //public static void GetSpriteGeometryFromFigure(Figure figure, out Vector2[] vertices, out ushort[] triangles)
    //{
    //    List<Vector3> listPoints = new List<Vector3>();
    //    for (int i = 0; i < figure.LinesFigure.Count; i++)
    //    {
    //        listPoints.Add(figure.LinesFigure[i].pointOne);
    //    }

    //    Triangulation.GetResult(listPoints, true, -Vector3.forward, out Vector3[] verticlesMesh, out int[] trianglesMesh, out Vector2[] uvcoords);
    //    triangles = new ushort[trianglesMesh.Length];
    //    for (int i = 0; i < trianglesMesh.Length; i += 3)
    //    {
    //        int ver = trianglesMesh[i + 1];
    //        trianglesMesh[i + 1] = trianglesMesh[i + 2];
    //        trianglesMesh[i + 2] = ver;
    //        triangles[i] = (ushort)trianglesMesh[i];
    //        triangles[i+1] = (ushort)trianglesMesh[i+1];
    //        triangles[i+2] = (ushort)trianglesMesh[i+2];
    //    }
    //    vertices = new Vector2[verticlesMesh.Length];
    //    for (int i = 0; i < vertices.Length; i++)
    //    {
    //        vertices[i] = new Vector2(verticlesMesh[i].x, verticlesMesh[i].y);
    //    }
    //}
}
