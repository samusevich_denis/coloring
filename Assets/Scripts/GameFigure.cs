﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFigure : MonoBehaviour
{
    private float time = 1;
    private ColorFigure colorFigure;
    private GamePictureSelect gamePictureSelect;
    private DataFigure dataFigure;

    static Coroutine coroutineMouseDownTimer;

    void Start()
    {
        gamePictureSelect = transform.parent.gameObject.GetComponent<GamePictureSelect>();
        colorFigure = GetComponent<ColorFigure>();
    }

    public void SetDataFigure(DataFigure dataFigure)
    {
        this.dataFigure = dataFigure;
    }
    public void SetMaterial(Material material)
    {
        if (dataFigure.isUnpainted)
        {
            return;
        }
        colorFigure.SetMaterial(material);
    }
    private void OnMouseDown()
    {
        coroutineMouseDownTimer = StartCoroutine(MouseDownTimer(time));
    }

    private void OnMouseUp()
    {
        if (coroutineMouseDownTimer!=null)
        {
            SelectFigure();
            return;
        }
       
    }
    private void SelectFigure()
    {
        if (dataFigure.isUnpainted)
        {
            return;
        }

        if (gamePictureSelect.SelectFigures(dataFigure.requiredСolorId))
        {
            dataFigure.isUnpainted = true;
            var color = gamePictureSelect.GetColor(dataFigure.requiredСolorId);
            colorFigure.SetMaterial(gamePictureSelect.standartMaterial);
            colorFigure.SetColor(color);
        }
    }

    private IEnumerator MouseDownTimer(float time)
    {
        yield return new WaitForSeconds(time);
        coroutineMouseDownTimer = null;
    }
}
